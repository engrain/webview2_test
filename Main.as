﻿package {
	import flash.display.MovieClip;
	import flash.media.StageWebView;
	import flash.geom.Rectangle;
	import flash.events.MouseEvent;

	public class Main extends MovieClip {

		private var webView: StageWebView;

		public function Main() {
			btn.mouseChildren = false;
			btn.addEventListener(MouseEvent.CLICK, btnHandler);
		}
		private function btnHandler(e: MouseEvent): void {
			webView = new StageWebView(true);
			webView.stage = this.stage;
			webView.viewPort = new Rectangle(0, 0, 800, 800);
			webView.loadURL("https://airsdk.harman.com/");
		}
	}
}